class User < ApplicationRecord
  has_many :videos
  has_many :reels
  has_many :users_clients
  has_many :clients, through: :users_clients
end
