class Client < ApplicationRecord
  has_many :users_clients
  has_many :user, through: :users_clients
  has_many :reels
  has_many :videos, through: :reels
end
