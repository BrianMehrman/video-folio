class Video < ApplicationRecord
  belongs_to :user
  has_many :reels_videos
  has_many :reels, through: :reels_videos

  mount_uploader :file, VideoUploader

  def set_success(format, opts)
    self.success = true
  end
end
