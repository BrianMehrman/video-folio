class Reel < ApplicationRecord
  belongs_to :user
  belongs_to :client
  has_many :reels_videos
  has_many :videos, through: :reels_videos
end
