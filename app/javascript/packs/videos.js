import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class Videos extends React.Component {
  render() {
    const { isAuthenticated } = this.props
    if (isAuthenticated) {
      return (
        <div>
          <h2>Videos</h2>
          <p>Videos Go here</p>
        </div>
      )
    }
    return (
      <div>
        <h2>Videos</h2>
        <p>Logged out</p>
      </div>
    )
  }
};

function mapStateToProps(state) {
  const { auth } = state;
  const { isAuthenticated } = auth;
  return { isAuthenticated };
};

export default connect(mapStateToProps)(Videos);
