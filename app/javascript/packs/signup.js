import { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      passwordConfirmation: ''
    };
  }

  handleSubmit() {
    const { name, email, password, passwordConfirmation } = this.state;
    const fd = new FormData();
    fd.append('name', name);
    fd.append('email', email);
    fd.append('password', password);
    fd.append('password_confirmation', passwordConfirmation);
    fd.append('confirm_success_url', '/confirm_success');

    fetch('/auth', {
      method: 'POST',
      header: { 'Content-Type': 'application/json' },
      body: fd
    })
    .then(res => res.json())
    .then(json => {
      console.log(json);
      console.log(document.cookie);
    });
  }

  render() {
    const { isAuthenticated } =  this.props;
    if (isAuthenticated) {
      return <Redirect to="/" />
    }
    return (
      <div>
        <h2>Signup</h2>
        <p>Name:
          <input type='text'
                 value={this.state.name}
                 onChange={(e) => this.setState({ name: e.target.value })} />
        </p>
        <p>Email:
          <input type='text'
                 value={this.state.email}
                 onChange={(e) => this.setState({ email: e.target.value })} />
        </p>
        <p>Password:
          <input type='text'
                 value={this.state.password}
                 onChange={(e) => this.setState({ password: e.target.value })} />
        </p>
        <p>Passowrd Confirmation:
          <input type='text'
                 value={this.state.passwordConfirmation}
                 onChange={(e) => this.setState({ passwordConfirmation: e.target.value})} />
        </p>
      </div>
    );
  }
};

function mapStateToProps(state) {
  const { auth } = state;
  const { isAuthenticated } = auth;
  return { isAuthenticated };
};

export default connect(mapStateToProps)(Signup);
