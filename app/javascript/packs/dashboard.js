// import React from 'react';
//
// import { render } from 'react-dom';
//
// import Dashboard from '../components/Dashboard';
//
// document.addEventListener('DOMContentLoaded', () => {
//   const container = document.body.appendChild(document.createElement('div'));
//   render(<Dashboard/>, container);
// });

import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Dashboard from '../components/Dashboard';

class _Dashboard extends React.Component {
  render() {
    const { isAuthenticated } = this.props;
    if (isAuthenticated) {
      return (<Dashboard />);
    }
    return (
      <div>
        <h2>Dashboard</h2>
        <p>Logged out</p>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { auth } = state;
  const { isAuthenticated } = auth;
  return { isAuthenticated };
}


export default connect(mapStateToProps)(_Dashboard);
