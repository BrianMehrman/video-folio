import '../styles'
import { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import PrivateRoute from '../components/utils/PrivateRoute'
import GlobalNavbar from '../components/GlobalNavbar'
import Videos from './videos'
import Signup from './signup'
import Login from './login'
import Dashboard from './dashboard'

class App extends Component {
  render() {
    return (
      <div>
        <GlobalNavbar  isOpen={true} />
        <div style={{ marginTop: 64 }}>
          <Route exact path="/" component={dashboard} />
          <Route path="/signup" component={Signup} />
          <Route path="/login" component={Login} />
          <Switch>
            <PrivateRoute exact path="/videos" component={Videos} />
          </Switch>
        </div>
      </div>
    )
  }
}

export default App;
