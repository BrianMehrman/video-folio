export const BASE_ROUTE = '/api';

export default class Client {
  constructor(props) {
    const { name, id, user_id } = props;
    this.name = name;
    this.id = id;
  }

  static get() {
    const request = new Request(`${BASE_ROUTE}/clients`, { method: 'GET' });

    return fetch(request)
      .then(response => response.json())
      .then(json => json.clients.map((client) => new Client(client)));
  }
}
