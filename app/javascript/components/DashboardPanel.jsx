import React, { Component } from 'react';

import { Container, Row, Col } from 'reactstrap';

/*
  Sidebar
    - My dashboard
    - My videos
    - My Reels
    - My Clients
  MainPanel
    - displays the current selected panel from the sidebar

*/
import ClientsPanel from './dashboard/ClientsPanel';
import MainPanel from './dashboard/MainPanel';
import ReelsPanel from './dashboard/ReelsPanel';
import VideosPanel from './dashboard/ReelsPanel';
import Sidebar from './dashboard/Sidebar';

export default class DashboardPanel extends Component {
  showPanel(selectedPanel) {
    return (
      <Col className="grey">
        { selectedPanel === 'clients' && <ClientsPanel /> }
        { selectedPanel === 'main' && <MainPanel /> }
        { selectedPanel === 'reels' && <ReelsPanel /> }
        { selectedPanel === 'videos' && <VideosPanel /> }
      </Col>
    )
  }

  render() {
    const selectedPanel = 'clients';
    return (
      <Container>
        <Row>
          <Col className="grey" xs="3">Sidebar</Col>
          { this.showPanel(selectedPanel) }
        </Row>
      </Container>
    )
  }
};
