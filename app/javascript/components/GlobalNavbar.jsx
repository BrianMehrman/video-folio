import { Component } from 'react';
import { connect } from 'react-redux';
import { signout } from '../modules/auth';

import {
  Alert,
  Row,
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';

class GlobalNavbar extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }

  signout(e) {
    e.preventDefault();
    this.props.dispatch(signout());
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    const { isAuthenticated } = this.props;
    return (
      <Container>
        <Navbar color="faded" light expand="md">
          <NavbarBrand href="/">Video Folio</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="/">Home</NavLink>
              </NavItem>
              { !isAuthenticated &&
                <NavItem>
                  <NavLink href="/signup">Signup</NavLink>
                </NavItem>
              }
              { !isAuthenticated &&
                <NavItem>
                  <NavLink href="/login">Login</NavLink>
                </NavItem>
              }
              { !isAuthenticated &&
                <NavItem>
                  <NavLink href="#" onClick={this.signout.bind(this)}>Signout</NavLink>
                </NavItem>
              }
            </Nav>
          </Collapse>
        </Navbar>
      </Container>
    )
  }
}

function mapStateToProps(state) {
  const { auth } = state;
  const { isAuthenticated } = auth;
  return { isAuthenticated };
}

export default connect(mapStateToProps)(GlobalNavbar);
