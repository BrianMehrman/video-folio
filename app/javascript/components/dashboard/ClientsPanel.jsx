import React, { Component } from 'react';
import {
  Button,
  ButtonGroup,
  Col,
  Container,
  ListGroup,
  ListGroupItem,
  Row
} from 'reactstrap';

import Client from '../../models/client';

export default class ClientsPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clients: Client.get()
    }
  }
  render() {
debugger
    const { clients } = this.state;
    return (
      <Container>
        <Row>
          <ButtonGroup>
            <Button>Add Client</Button>
          </ButtonGroup>
        </Row>
        <Row>
          <ListGroup>
            {
              (clients || []).map((client) => {
                return (
                  <ListGroupItem>
                    {client}
                  </ListGroupItem>
                )
              })
            }
          </ListGroup>
        </Row>
      </Container>
    )
  }
}
