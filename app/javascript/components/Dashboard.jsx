import React, { Component } from 'react';
import { Alert } from 'reactstrap';

import MainNavbar from './MainNavbar';
import DashboardPanel from './DashboardPanel';

export default class Dashboard extends Component {
  render() {
    return (
      <div className="row">
        <MainNavbar isOpen={true} alerts={['Test alert message.']} />
        <DashboardPanel />
      </div>
    );
  }
}
