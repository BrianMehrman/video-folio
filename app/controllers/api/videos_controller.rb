class VideosController < ApplicationController
  def index
    render json: data.to_json
  end

  private

  def data
    [
      { name: 'Rain', id: 'rain-123', user_id: 'abc-123' },
      { name: 'Clouds', id: 'cloud-124', user_id: 'abc-122' },
      { name: 'Trees', id: 'tree-125', user_id: 'abc-125' }
    ]
  end
end
