class ClientsController < ApplicationController
  def index
    render json: data.to_json
  end

  private

  def data
    [
      { name: 'foo', id: 'zxy-123', user_id: 'abc-123' },
      { name: 'bar', id: 'zxy-124', user_id: 'abc-122' },
      { name: 'bash', id: 'zxy-125', user_id: 'abc-125' }
    ]
  end
end
