class CreateVideos < ActiveRecord::Migration[5.1]
  def change
    create_table :videos, id: :uuid do |t|
      t.uuid :user_id
      t.string :file
      t.timestamps
    end
  end
end
