class CreateReelsVideos < ActiveRecord::Migration[5.1]
  def change
    create_table :reels_videos, id: :uuid do |t|
      t.uuid :video_id
      t.uuid :reel_id
      t.uuid :user_id
      t.timestamps
    end
  end
end
