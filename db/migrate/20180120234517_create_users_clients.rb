class CreateUsersClients < ActiveRecord::Migration[5.1]
  def change
    create_table :users_clients, id: :uuid do |t|
      t.uuid :user_id
      t.uuid :client_id
      t.timestamps
    end
  end
end
