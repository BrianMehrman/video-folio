class CreateReels < ActiveRecord::Migration[5.1]
  def change
    create_table :reels, id: :uuid do |t|
      t.string :name
      t.uuid :user_id
      t.uuid :client_id
      t.timestamps
    end
  end
end
