# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

admin = User.create(email: 'admin@test.com', password: 'cat-123', password_confirmation: 'cat-123')
user_a = User.create(email: 'bob@test.com', password: 'password', password_confirmation: 'password')

videos = [
  {
    
  }
]

Videos.create(videos)
