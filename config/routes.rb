Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    resources :clients, only: [:index]
  end

  get '/dashboard', to: 'dashboard#index'
  get '/signup', to: 'dashboard#index'
  get '/login', to: 'dashboad#index'

  root 'dashboard#index'
end
